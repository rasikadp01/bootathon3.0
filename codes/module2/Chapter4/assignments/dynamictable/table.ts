function tab() {
    var num1:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    var table:HTMLTableElement=<HTMLTableElement>document.getElementById("tb1");
    var i:number=1;
    var num:number=+num1.value;
    while (table.rows.length>1)
    {
        table.deleteRow(1);
    }
    for (i=1; i<=num; i++)
    {
        var row:HTMLTableRowElement = table.insertRow();
        var column:HTMLTableDataCellElement = row.insertCell();
        var t:HTMLInputElement = document.createElement("input");
        t.type = "text";
        t.style.backgroundColor = "#60D1F2";
        t.style.textAlign = "center";
        t.value = num.toString()+" * "+i.toString();
        column.appendChild(t);
        
        var column:HTMLTableDataCellElement = row.insertCell();
        var t:HTMLInputElement = document.createElement("input");
        t.type = "text";
        t.style.backgroundColor = "#60D1F2";
        t.style.textAlign = "center";
        t.value = (num*i).toString();
        column.appendChild(t);
    }
}
