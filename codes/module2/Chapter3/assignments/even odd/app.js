function eveodd() {
    let t1 = document.getElementById("t1");
    var a = parseFloat(t1.value);
    if (isNaN(a)) {
        alert("Please enter a number");
    }
    else {
        if (a % 2 == 0) {
            document.getElementById("p1").innerHTML = "It is an even number";
        }
        else {
            document.getElementById("p1").innerHTML = "It is odd number";
        }
    }
}
//# sourceMappingURL=app.js.map