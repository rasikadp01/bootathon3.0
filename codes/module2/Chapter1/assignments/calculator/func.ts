function add()
{
    let t1:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    let t2:HTMLInputElement=<HTMLInputElement>document.getElementById("t2");
    var c:number=parseFloat(t1.value);
    var d:number=parseFloat(t2.value);
    var res:number=c+d;
    document.getElementById("p1").innerHTML+="The addition is : </br>"+res.toString()+"</br>";
}

function sub()
{
    let t1:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    let t2:HTMLInputElement=<HTMLInputElement>document.getElementById("t2");
    var c:number=parseFloat(t1.value);
    var d:number=parseFloat(t2.value);
    var res:number=c-d;
    document.getElementById("p1").innerHTML+="The Subtraction is : </br>"+res.toString()+"</br>";
}

function multi()
{
    let t1:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    let t2:HTMLInputElement=<HTMLInputElement>document.getElementById("t2");
    var c:number=parseFloat(t1.value);
    var d:number=parseFloat(t2.value);
    var res:number=c*d;
    document.getElementById("p1").innerHTML+="The Multiplication is : </br>"+res.toString()+"</br>";
}

function div()
{
    let t1:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    let t2:HTMLInputElement=<HTMLInputElement>document.getElementById("t2");
    var c:number=parseFloat(t1.value);
    var d:number=parseFloat(t2.value);
    var res:number=c/d;
    document.getElementById("p1").innerHTML+="The Division is : </br>"+res.toString()+"</br>";
}

function sine()
{
    let t3:HTMLInputElement=<HTMLInputElement>document.getElementById("t3");
    var c:number=parseFloat(t3.value);
    var c1:number=c*Math.PI/180;
    var res:number=Math.sin(c1);
    document.getElementById("p1").innerHTML+="The sine is : </br>"+res.toString()+"</br>";
}

function cosine()
{
    let t3:HTMLInputElement=<HTMLInputElement>document.getElementById("t3");
    var c:number=parseFloat(t3.value);
    var c1:number=c*Math.PI/180;
    var res:number=Math.cos(c1);
    document.getElementById("p1").innerHTML+="The cosine is : </br>"+res.toString()+"</br>";
}

function tangent()
{
    let t3:HTMLInputElement=<HTMLInputElement>document.getElementById("t3");
    var c:number=parseFloat(t3.value);
    var c1:number=c*Math.PI/180;
    var res:number=Math.tan(c1);
    document.getElementById("p1").innerHTML+="The tangent is : </br>"+res.toString()+"</br>";
}

function Sqrt()
{
    let t3:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    var c:number=parseFloat(t3.value);
    var res:number=Math.sqrt(c);
    document.getElementById("p1").innerHTML+="The square root is : </br>"+res.toString()+"</br>";
}

function Pow()
{
    let t1:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    let t2:HTMLInputElement=<HTMLInputElement>document.getElementById("t2");
    var c:number=parseFloat(t1.value);
    var d:number=parseFloat(t2.value);
    var res:number=Math.pow(c,d);
    document.getElementById("p1").innerHTML+="The power is : </br>"+res.toString()+"</br>";
}