function add() {
    let t1 = document.getElementById("t1");
    let t2 = document.getElementById("t2");
    var c = parseFloat(t1.value);
    var d = parseFloat(t2.value);
    var res = c + d;
    document.getElementById("p1").innerHTML += "The addition is : </br>" + res.toString() + "</br>";
}
function sub() {
    let t1 = document.getElementById("t1");
    let t2 = document.getElementById("t2");
    var c = parseFloat(t1.value);
    var d = parseFloat(t2.value);
    var res = c - d;
    document.getElementById("p1").innerHTML += "The Subtraction is : </br>" + res.toString() + "</br>";
}
function multi() {
    let t1 = document.getElementById("t1");
    let t2 = document.getElementById("t2");
    var c = parseFloat(t1.value);
    var d = parseFloat(t2.value);
    var res = c * d;
    document.getElementById("p1").innerHTML += "The Multiplication is : </br>" + res.toString() + "</br>";
}
function div() {
    let t1 = document.getElementById("t1");
    let t2 = document.getElementById("t2");
    var c = parseFloat(t1.value);
    var d = parseFloat(t2.value);
    var res = c / d;
    document.getElementById("p1").innerHTML += "The Division is : </br>" + res.toString() + "</br>";
}
function sine() {
    let t3 = document.getElementById("t3");
    var c = parseFloat(t3.value);
    var c1 = c * Math.PI / 180;
    var res = Math.sin(c1);
    document.getElementById("p1").innerHTML += "The sine is : </br>" + res.toString() + "</br>";
}
function cosine() {
    let t3 = document.getElementById("t3");
    var c = parseFloat(t3.value);
    var c1 = c * Math.PI / 180;
    var res = Math.cos(c1);
    document.getElementById("p1").innerHTML += "The cosine is : </br>" + res.toString() + "</br>";
}
function tangent() {
    let t3 = document.getElementById("t3");
    var c = parseFloat(t3.value);
    var c1 = c * Math.PI / 180;
    var res = Math.tan(c1);
    document.getElementById("p1").innerHTML += "The tangent is : </br>" + res.toString() + "</br>";
}
function Sqrt() {
    let t3 = document.getElementById("t1");
    var c = parseFloat(t3.value);
    var res = Math.sqrt(c);
    document.getElementById("p1").innerHTML += "The square root is : </br>" + res.toString() + "</br>";
}
function Pow() {
    let t1 = document.getElementById("t1");
    let t2 = document.getElementById("t2");
    var c = parseFloat(t1.value);
    var d = parseFloat(t2.value);
    var res = Math.pow(c, d);
    document.getElementById("p1").innerHTML += "The power is : </br>" + res.toString() + "</br>";
}
//# sourceMappingURL=func.js.map