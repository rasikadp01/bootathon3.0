## Aim    

 To study lagrange's Interpolation.
 
## Theory  
 * ### Lagrange's Interpolation
Lagrange's Interpolation is used mainly for polynomial interpolation
.It can be used for unequally spaced points as well as equally spaced points.When constructing polynomial there is a trade-off between a better fit and having smooth well behaved fitting function.Higher the number of data points ,higher is the degree of polynomial fitted therfore a greater osscilation it will exhibit between data points . Therfore higher degree interpolation maybe poor indicator of function between points although the accuracy of data points is perfect.  
 * ### formula for lagrange's interpolation is:  
Formula for Lagrange's Interpolation is:
![image2](images/image2.png)


Example To find y(1.5) use Lagrange's interpolation  

|x|1|2|3|  
|---|---|---|---|---|
|y|1|4|9|


L(0)=[(1.5-2)*(1.5-3)]/[(1-2)*(1-3)]  L(0)=0.375  
L(1)=[(1.5-1)*(1.5-3)]/[(2-1)*(2-3)]  L(1)=0.75  
L(2)=[(1.5-1)*(1.5-2)]/[(3-1)*(3-2)]  
L(2)=-0.125  
y(1.5)=0.375*1+0.75*4-0.125*9=2.25 

## Procedure:
 * ### Example 
Find Y(2.5)for following data   


|X|1|2|3|4|5|
|----|---|---|---|---|---|---|
|Y|1|4|9|16|25|



 * Enter the data in the following format and hit solve  


![image1](images/image1.jpg)